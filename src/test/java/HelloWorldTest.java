import org.junit.Test;
import org.junit.Assert;

public class HelloWorldTest {
    private HelloWorld hello = new HelloWorld();

    @Test
    public void test() {
        Assert.assertEquals("Hello, JC", hello.greeting("JC"));
    }
}
