pipeline {
    agent any
    environment {
        PROJECT_TYPE = 'maven'
    }
    stages {
        stage('Build') {
            steps {
                bat 'mvn package'
            }
        }

        stage('Test') {
            steps {
                bat 'mvn clean test'
            }
            post {
                always {
                    junit "**/target/surefire-reports/*.xml"
                }
           }
        }

        stage('Code Analysis') {
            steps {
                // bat 'mvn clean package sonar:sonar'
                
                bat 'mvn verify org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.projectKey=ndncoadmin_java-web-project -Dsonar.login=%SONAR_TOKEN%'

                // echo 'Emulating code analysis in Sonar.'
            }
        }

        stage('Deploy') {
            environment {
                APP_NAME = 'java-web-project'
            }
            steps {
                echo 'ServiceNow Change Process executing.'

                snDevOpsArtifact(artifactsPayload: """{"artifacts": [{"name": "java-web-project.war", "version": "1.${env.BUILD_NUMBER}","semanticVersion": "1.${env.BUILD_NUMBER}.0","repositoryName": "java-web-project"}], "branchName": "master", "stageName": "Deploy"}""")

                snDevOpsPackage(name: "sentimentpackage", artifactsPayload: """{"artifacts":[{"name": "java-web-project.war", "version": "1.${env.BUILD_NUMBER}","repositoryName": "java-web-project"}], "branchName": "master", "stageName": "Deploy"}""")

                snDevOpsChange()


                echo 'Deploying to Tomcat server.'
                bat 'mvn install tomcat:redeploy'
            }
        }
    }

    tools {
        maven 'M3'
    }

    post {
        always {
            echo 'Archiving the artifacts.'
            // This works but can comment it to prevent storage issues.
            archiveArtifacts artifacts: 'target/*.war', onlyIfSuccessful: true
        }
    }
}
